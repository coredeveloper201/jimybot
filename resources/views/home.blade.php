@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Registered Users</div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @foreach($users->where('role', 'user') as $user)
                        <li class="list-group-item">
                            <h4>Name: {{ $user->name }}</h4>
                            <strong>Email: {{ $user->email }}</strong><small>  Phone: {{ $user->phone }}</small>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Books Ait Tickets</div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @foreach($books as $book)
                        <li class="list-group-item">
                            <h4>Plane Name: {{ $book->name }}</h4>
                            <strong>Name:</strong> {{ $book->username }}  <strong>Email:</strong> {{ $book->email }} <strong>Phone:</strong> {{ $book->phone }} <br>
                            <small>Date: {{ $book->date }}  Time: {{ $book->time }}</small>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
