<?php
use App\Http\Controllers\BotManController;

$botman = resolve('botman');

$botman->hears('Hi|Hello', BotManController::class.'@startConversation');

$botman->hears('call me {name}', function ($bot, $name) {
    $bot->reply('Your name is: '.$name);
});

$botman->hears('.*Your name*', function ($bot) {
    $bot->reply('I am Jimy.!');
});

$botman->fallback(BotManController::class.'@getLost');
