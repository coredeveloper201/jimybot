<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class SelectServiceConversation extends Conversation
{
    public function askService()
    {
        $question = Question::create('Choose Airway?')
            ->callbackId('select_service')
            ->addButtons([
                Button::create('Biman Bangladesh')->value('Biman Bangladesh'),
                Button::create('Qatar airways')->value('Qatar airways'),
                Button::create('Malasian Airways')->value('Malasian Airways'),
            ]);

        $this->ask($question, function(Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->userStorage()->save([
                    'planename' => $answer->getValue(),
                ]);
                $this->bot->startConversation(new DateTimeConversation());
            }
        });
    }

    public function run()
    {
        $this->askService();
    }
}
