<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\BotMan\Messages\Conversations\Conversation;

class BackTrackConversation extends Conversation
{
    public function mainOption()
    {
        $question = Question::create('Choose a track')
            ->callbackId('select_service')
            ->addButtons([
                Button::create('Book AirPlane Ticket.')->value('BA'),
                Button::create('Book Train Ticket.')->value('BT'),
            ]);
        
        $this->say('Maybe you lost.. <br><br>');
        $this->ask($question, function(Answer $answer) {
            if ($answer->isInteractiveMessageReply()) {
                $this->bot->userStorage()->save([
                    'register' => $answer->getValue(),
                ]);
                    
                if($answer->getValue() == 'BA'){
                    $this->bot->startConversation(new GreetingsConversation());
                }elseif($answer->getValue() == 'BT'){
                    $this->bot->startConversation(new GreetingsConversation());
                }else{
                    return $this->repeat($this->mainOption());
                }
            }
        });
        $this->say('Or ask me my name. <br><br>');
        }
    public function run()
    {
        $this->mainOption();
    }
}
