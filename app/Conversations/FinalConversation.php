<?php

namespace App\Conversations;
use App\Book;

use BotMan\BotMan\Messages\Conversations\Conversation;

class FinalConversation extends Conversation
{
    public function confirmBooking()
    {
        $user = $this->bot->userStorage()->find();

        $book = new Book;
        $book->user = $user->get('id') ? $user->get('id') : 'Guest';
        $book->name = $user->get('planename');
        $book->username = $user->get('name');
        $book->email = $user->get('name');
        $book->phone = $user->get('name');
        $book->date = $user->get('date');
        $book->time = $user->get('timeSlot');
        $book->save();

        $message = '-------------------------------------- <br>';
        $message .= 'Name : ' . $user->get('name') . '<br>';
        $message .= 'Email : ' . $user->get('email') . '<br>';
        $message .= 'Mobile : ' . $user->get('mobile') . '<br>';
        $message .= 'Plane Name: ' . $user->get('planename') . '<br>';
        $message .= 'Date : ' . $user->get('date') . '<br>';
        $message .= 'Time : ' . $user->get('timeSlot') . '<br>';
        $message .= '---------------------------------------';

        $this->say('Successfully Confirmed. Check your mail. <br><br>' . $message);
    }

    public function run()
    {
        $this->confirmBooking();
    }
}
